package com.example.pointlessapprebirth;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Snackbar.make(findViewById(R.id.main), R.string.plum, Snackbar.LENGTH_SHORT).show();

    }

    public void runAbout(View view) {
        final Intent runAb = new Intent(this, About.class);
        startActivity(runAb);
        finishAffinity();

        Toast.makeText(MainActivity.this, "PLUM",
                Toast.LENGTH_SHORT).show();
    }

    public void runForm(View view) {
        final Intent runF = new Intent(this, Form.class);
        startActivity(runF);
        finishAffinity();

        Toast.makeText(MainActivity.this, R.string.formInfo,
                Toast.LENGTH_LONG).show();
    }

    public void runExit(View view) {
        finishAffinity();
        System.exit(0);

    }
}
