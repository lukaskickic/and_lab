package com.example.pointlessapprebirth;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
    public void runPointless(View view) {
        final Intent intencja1 = new Intent(this, MainActivity.class);
        startActivity(intencja1);
        finishAffinity();
    }

    public void runBefore(View view) {
        final Intent runB = new Intent(this, BeforeStart.class);
        startActivity(runB);
    }
}
